See below for English description.

# Suomennos Auto Groups -laajennukselle

Suomenkielinen kielipaketti phpBB:n Auto Groups -laajennukselle.

## Asennus

1. Asenna Auto Groups -laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-2.0.0.zip)
	- [1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1.0.2.zip)
	- [1.0.1](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1_0_1.7z)
	- [1.0.0](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1_0_0.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/autogroups`-hakemistoon.

# Finnish Translation for Auto Groups

A Finnish language pack for the Auto Groups extension for phpBB.

## Installation

1. Install the Auto Groups extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [2.0.0](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-2.0.0.zip)
	- [1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1.0.2.zip)
	- [1.0.1](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1_0_1.7z)
	- [1.0.0](https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb/downloads/auto-groups-fi-1_0_0.7z)

3. Extract the contents of the archive into the `ext/phpbb/autogroups`
directory under your phpBB root directory.
