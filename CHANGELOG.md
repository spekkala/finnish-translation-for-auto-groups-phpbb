# Change Log

## 2.0.0 (2017-02-26)

- Include extension version 2.0.0 in `README.md`.

## 1.0.2 (2017-02-26)

- Include extension version 1.0.2 in `README.md`.

## 1.0.1 (2016-01-14)

- Update translations to be compatible with Auto Groups 1.0.1.

## 1.0.0 (2015-10-06)

- Initial release.
