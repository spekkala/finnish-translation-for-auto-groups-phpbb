<?php
/**
*
* Auto Groups extension for the phpBB Forum Software package.
* Finnish translation by Sami Pekkala (https://bitbucket.org/spekkala/finnish-translation-for-auto-groups-phpbb)
*
* @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'ACP_AUTOGROUPS_MANAGE'			=> 'Automaattiset ryhmät',
	'ACP_AUTOGROUPS_MANAGE_EXPLAIN'	=> 'Tässä voit luoda, muokata, katsoa ja poistaa automaattisia ryhmiä.',
	'ACP_AUTOGROUPS_ADD'			=> 'Luo automaattinen ryhmä',
	'ACP_AUTOGROUPS_EDIT'			=> 'Muokkaa automaattista ryhmää',

	'ACP_AUTOGROUPS_GROUP_NAME'				=> 'Ryhmä',
	'ACP_AUTOGROUPS_GROUP_NAME_EXPLAIN'		=> 'Valitse ryhmä, johon käyttäjät lisätään tai poistetaan automaattisesti.',
	'ACP_AUTOGROUPS_CONDITION_NAME'			=> 'Automaattisen ryhmän tyyppi',
	'ACP_AUTOGROUPS_CONDITION_NAME_EXPLAIN'	=> 'Valitse, minkälaisen ehdon perusteella käyttäjät lisätään tai poistetaan tästä ryhmästä.',
	'ACP_AUTOGROUPS_MIN_VALUE'				=> 'Vähimmäisarvo',
	'ACP_AUTOGROUPS_MIN_VALUE_EXPLAIN'		=> 'Käyttäjät lisätään tähän ryhmään, jos he saavuttavat vähimmäisarvon.',
	'ACP_AUTOGROUPS_MAX_VALUE'				=> 'Enimmäisarvo',
	'ACP_AUTOGROUPS_MAX_VALUE_EXPLAIN'		=> 'Käyttäjät poistetaan tästä ryhmästä, jos he ylittävät enimmäisarvon. Jos kenttä on tyhjä, käyttäjiä ei poisteta.',
	'ACP_AUTOGROUPS_DEFAULT'				=> 'Aseta oletusryhmäksi',
	'ACP_AUTOGROUPS_DEFAULT_EXPLAIN'		=> 'Aseta tämä ryhmä käyttäjien uudeksi oletusryhmäksi.',
	'ACP_AUTOGROUPS_NOTIFY'					=> 'Ilmoita käyttäjille',
	'ACP_AUTOGROUPS_NOTIFY_EXPLAIN'			=> 'Lähetä ilmoitus käyttäjille, jos heidät lisätään tai poistetaan tästä ryhmästä automaattisesti.',

	'ACP_AUTOGROUPS_EXEMPT_GROUP'			=> 'Oletusryhmiä koskevat vapautukset',
	'ACP_AUTOGROUPS_EXEMPT_GROUP_EXPLAIN'	=> 'Automaattisten ryhmien asetukset eivät muuta käyttäjän oletusryhmää, jos ryhmä on merkitty valituksi tässä luettelossa. Voit valita useita ryhmiä pitämällä pohjassa <samp>CTRL</samp>-näppäintä (tai <samp>&#8984;CMD</samp>-näppäintä Mac-tietokoneilla).',

	'ACP_AUTOGROUPS_CREATE_RULE'	=> 'Luo uusi automaattinen ryhmä',
	'ACP_AUTOGROUPS_SUBMIT_SUCCESS'	=> 'Automaattisen ryhmän asetukset on tallennettu.',
	'ACP_AUTOGROUPS_DELETE_CONFIRM'	=> 'Haluatko varmasti poistaa tämän automaattisen ryhmän?',
	'ACP_AUTOGROUPS_DELETE_SUCCESS'	=> 'Automaattinen ryhmä on poistettu.',
	'ACP_AUTOGROUPS_EMPTY'			=> 'Automaattisia ryhmiä ei ole.',
	'ACP_AUTOGROUPS_NO_GROUPS'		=> 'Ryhmiä ei ole',
	'ACP_AUTOGROUPS_INVALID_GROUPS'	=> 'Virhe: Valittu käyttäjäryhmä ei ole kelvollinen.<br />Vain käyttäjän määrittelemistä ryhmistä voi tehdä automaattisia ryhmiä. Uusia ryhmiä voi luoda Ryhmien hallinta -sivulla.',
	'ACP_AUTOGROUPS_INVALID_RANGE'	=> 'Virhe: Vähimmäis- ja enimmäisarvot eivät voi olla samansuuruisia.',

	// Conditions
	'AUTOGROUPS_TYPE_BIRTHDAYS'		=> 'Käyttäjän ikä vuosina',
	'AUTOGROUPS_TYPE_MEMBERSHIP'	=> 'Jäsenyyden kesto päivinä',
	'AUTOGROUPS_TYPE_POSTS'			=> 'Viestien lukumäärä',
	'AUTOGROUPS_TYPE_WARNINGS'		=> 'Varoitusten lukumäärä',
));
